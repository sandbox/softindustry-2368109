<?php
/**
 * @file
 * Implementation of HTTP request class.
 */

/**
 * HTTP Request.
 *
 * Object oriented version of amazing drupal_http_request().
 * function.
 *
 * @author Skorobogatko Alexei <skoro@soft-industry.com>
 */
class BackupMigrateWebDAVHTTPRequest {

  /**
   * Error code indicating that the request exceeded the specified timeout.
   */
  const HTTP_REQUEST_TIMEOUT = -1;

  /**
   * Request method (GET, POST, etc).
   *
   * @var string
   */
  public $method = 'POST';

  /**
   * Request data.
   *
   * @var string
   */
  public $data = '';

  /**
   * HTTP headers.
   *
   * @var array
   */
  public $headers = array(
    'User-Agent' => 'PHP_HTTP_Request',
  );

  /**
   * Request options.
   *
   * Options:
   * <dl>
   *  <dt>timeout</dt> <dd>request timeout, default 30 sec</dd>
   *  <dt>max_redirects</dt> <dd>max count of redirects, default 3</dd>
   *  <dt>context</dt> <dd>socket context options stream_context_create()</dd>
   * <dl>
   *
   * @var array
   */
  public $options = array();

  /**
   * HTTP response status codes.
   *
   * @var array
   */
  public static $responses = array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Time-out',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Large',
    415 => 'Unsupported Media Type',
    416 => 'Requested range not satisfiable',
    417 => 'Expectation Failed',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Time-out',
    505 => 'HTTP Version not supported',
  );

  /**
   * Socket connection string.
   *
   * @var string
   */
  protected $socket = '';

  /**
   * Stream handle.
   *
   * @var resource
   */
  protected $handle = NULL;

  /**
   * Url splitted on parts: scheme, host, port.
   *
   * @var array
   */
  protected $uri = array();

  /**
   * Request timeout value.
   *
   * @var array
   */
  protected $timer = array();

  /**
   * Create new instance.
   *
   * @param string $url
   *   Request url.
   * @param array $options
   *   Request options.
   */
  public function __construct($url, array $options = array()) {
    $this->setUrl($url);
    $this->options = array_merge($this->options, $options);
  }

  /**
   * Add HTTP header to request.
   *
   * @param string $header
   *   HTTP header name.
   * @param string $value
   *   Header value.
   *
   * @return $this
   *   Self instance for method chaining.
   */
  public function addHeader($header, $value) {
    $this->headers[$header] = $value;
    return $this;
  }

  /**
   * Remove HTTP header.
   *
   * @param string $header
   *   HTTP header name.
   *
   * @return $this
   *   Self instance for method chaining.
   */
  public function removeHeader($header) {
    if (isset($this->headers[$header])) {
      unset($this->headers[$header]);
    }
    return $this;
  }

  /**
   * Set request url.
   *
   * @param string $url
   *   Request url.
   *
   * @throws Exception
   *   On parse error.
   *
   * @todo Create classes for Exceptions: HTTPRequest\SchemaException,
   *       HTTPRequest\UrlException.
   */
  public function setUrl($url) {
    if (!($uri = @parse_url($url))) {
      // TODO: HTTP_Request\UrlException.
      throw new Exception('Unable to parse url.');
    }

    if (!isset($uri['scheme'])) {
      // TODO: HTTP_Request\SchemaException.
      throw new Exception('Missing schema.');
    }

    switch ($uri['scheme']) {
      case 'http':
        $port = isset($uri['port']) ? $uri['port'] : 80;
        $this->socket = 'tcp://' . $uri['host'] . ':' . $port;
        // RFC 2616: "non-standard ports MUST, default ports MAY be included".
        // We don't add the standard port to prevent from breaking rewrite rules
        // checking the host that do not take into account the port number.
        $this->addHeader('Host', $uri['host'] . ($port != 80 ? ':' . $port : ''));
        break;

      case 'https':
        // Note: Only works when PHP is compiled with OpenSSL support.
        $port = isset($uri['port']) ? $uri['port'] : 443;
        $this->socket = 'ssl://' . $uri['host'] . ':' . $port;
        $this->addHeader('Host', $uri['host'] . ($port != 443 ? ':' . $port : ''));
        break;

      default:
        // TODO: HTTP_Request\SchemaException.
        throw new Exception('Invalid schema ' . $uri['scheme']);
    }

    $this->uri = $uri;
  }

  /**
   * Create socket connection and send request headers.
   *
   * @throws Exception
   *   On connection error.
   */
  protected function prepareRequest() {

    // Check parsed url.
    if (empty($this->socket)) {
      // TODO: SocketException.
      throw new Exception('Socket cannot be initialized.');
    }

    // Default options.
    $this->options += array(
      'max_redirects' => 3,
      'timeout' => 30.0,
      'context' => NULL,
    );

    // Initialize socket timer.
    $this->timer = microtime(TRUE);

    // Establish socket connection.
    if (empty($this->options['context'])) {
      $this->handle = @stream_socket_client($this->socket, $errno, $errstr,
          $this->options['timeout']);
    }
    else {
      // Create a stream with context. Allows verification of a SSL certificate.
      $this->handle = @stream_socket_client($this->socket, $errno, $errstr,
          $this->options['timeout'], STREAM_CLIENT_CONNECT, $this->options['context']);
    }

    if (!$this->handle) {
      $errstr = trim($errstr) ? trim($errstr) : 'Error opening socket "' . $this->socket . '"';
      throw new Exception($errstr);
    }

    // Construct the path to act on.
    $path = isset($this->uri['path']) ? $this->uri['path'] : '/';
    if (isset($this->uri['query'])) {
      $path .= '?' . $this->uri['query'];
    }

    // Only add Content-Length if we actually have any content or if it is a
    // POST or PUT request. Some non-standard servers get confused by
    // Content-Length in at least HEAD/GET requests, and Squid always requires
    // Content-Length in POST/PUT requests.
    $content_length = strlen($this->data);
    $this->method = strtoupper($this->method);
    if (($content_length > 0 && !isset($this->headers['Content-Length'])) &&
        ($this->method === 'POST' || $this->method === 'PUT')) {
      $this->headers['Content-Length'] = $content_length;
    }

    // If the server URL has a user then attempt to use basic authentication.
    if (isset($this->uri['user'])) {
      $this->headers['Authorization'] = 'Basic ' . base64_encode($this->uri['user']
          . (isset($this->uri['pass']) ? ':' . $this->uri['pass'] : ':'));
    }

    $request = $this->method . ' ' . $path . " HTTP/1.0\r\n";
    foreach ($this->headers as $name => $value) {
      $request .= $name . ': ' . trim($value) . "\r\n";
    }

    $request .= "\r\n";

    // Calculate how much time is left of the original timeout value.
    $timeout = $this->options['timeout'] - $this->timerRead() / 1000;
    if ($timeout > 0) {
      stream_set_timeout($this->handle, floor($timeout), floor(1000000 * fmod($timeout, 1)));
      fwrite($this->handle, $request);
    }
  }

  /**
   * Read request response.
   *
   * @return object
   *   Response object.
   */
  protected function response() {
    // Response object.
    $result = new stdClass();

    // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
    // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
    // instead must invoke stream_get_meta_data() each iteration.
    $info = stream_get_meta_data($this->handle);
    $alive = !$info['eof'] && !$info['timed_out'];
    $response = '';

    while ($alive) {
      // Calculate how much time is left of the original timeout value.
      $timeout = $this->options['timeout'] - $this->timerRead() / 1000;
      if ($timeout <= 0) {
        $info['timed_out'] = TRUE;
        break;
      }
      stream_set_timeout($this->handle, floor($timeout), floor(1000000 * fmod($timeout, 1)));
      $chunk = fread($this->handle, 1024);
      $response .= $chunk;
      $info = stream_get_meta_data($this->handle);
      $alive = !$info['eof'] && !$info['timed_out'] && $chunk;
    }
    fclose($this->handle);

    // Failed by timeout.
    if ($info['timed_out']) {
      $result->code = self::HTTP_REQUEST_TIMEOUT;
      $result->error = 'request timed out';
      return $result;
    }

    // Parse response headers from the response body.
    // Be tolerant of malformed HTTP responses that separate header and body
    // with \n\n or \r\r instead of \r\n\r\n.
    list($response, $result->data) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
    $response = preg_split("/\r\n|\n|\r/", $response);

    // Parse the response status line.
    list($protocol, $code, $status_message) = explode(' ', trim(array_shift($response)), 3);
    $result->protocol = $protocol;
    $result->status_message = $status_message;

    $result->headers = array();

    // Parse the response headers.
    while ($line = trim(array_shift($response))) {
      list($name, $value) = explode(':', $line, 2);
      $name = strtolower($name);
      if (isset($result->headers[$name]) && $name == 'set-cookie') {
        // RFC 2109: the Set-Cookie response header comprises the token Set-
        // Cookie:, followed by a comma-separated list of one or more cookies.
        $result->headers[$name] .= ',' . trim($value);
      }
      else {
        $result->headers[$name] = trim($value);
      }
    }
    // RFC 2616 states that all unknown HTTP codes must be treated the same as
    // the base code in their class.
    if (!isset(self::$responses[$code])) {
      $code = floor($code / 100) * 100;
    }
    $result->code = $code;

    switch ($code) {
      case 200:
        // OK.
      case 304:
        // Not modified.
        break;

      default:
        $result->error = $status_message;
    }

    return $result;
  }

  /**
   * Perform HTTP request.
   *
   * @param string $data
   *   Request data.
   *
   * @throws Exception
   *   Connection error occurs.
   *
   * @return object
   *   Response object.
   */
  public function sendRequest($data = NULL) {
    $data = ($data === NULL) ? $this->data : $data;
    $this->prepareRequest();
    fwrite($this->handle, $data);

    $response = $this->response();
    $code = $response->code;

    switch ($code) {
      case 301:
        // Moved permanently.
      case 302:
        // Moved temporarily.
      case 307:
        // Moved temporarily.
        $location = $response->headers['location'];
        $this->options['timeout'] -= $this->timerRead() / 1000;
        if ($this->options['timeout'] <= 0) {
          $response->code = self::HTTP_REQUEST_TIMEOUT;
          $response->error = 'request timed out';
        }
        elseif ($this->options['max_redirects']) {
          // Redirect to the new location.
          $this->options['max_redirects']--;
          $this->setUrl($location);
          $this->sendRequest();
          $response->redirect_code = $code;
        }
        if (!isset($response->redirect_url)) {
          $response->redirect_url = $location;
        }
        break;
    }

    return $response;
  }

  /**
   * Copy data to socket stream.
   *
   * @param string $data
   *   Data to be written.
   *
   * @return int|FALSE
   *   Count of data written or FALSE on error.
   */
  public function streamCopy($data) {
    if ($this->handle === NULL) {
      $this->prepareRequest();
    }

    $count = fwrite($this->handle, $data, strlen($data));
    return $count;
  }

  /**
   * Close opened connection.
   *
   * @return object|NULL
   *   Response instance or NULL on error.
   */
  public function reset() {
    $response = NULL;

    if (is_resource($this->handle)) {
      $response = $this->response();
      $this->handle = NULL;
    }

    return $response;
  }

  /**
   * Time difference to measure timeouts.
   *
   * @return float
   *   Time difference in milliseconds.
   *
   * @todo Implement this as PHP trait/class for using in other classes too.
   */
  protected function timerRead() {
    $stop = microtime(TRUE);
    $diff = round(($stop - $this->timer) * 1000, 2);
    return $diff;
  }

}
