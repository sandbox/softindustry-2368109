<?php

/**
 * @file
 * Functions to handle the WebDAV backup destination.
 */

/**
 * A destination for sending database backups to the WebDAV service.
 *
 * @ingroup backup_migrate_destinations
 */
class BackupMigrateWebDAVDestination extends backup_migrate_destination_remote {

  public $supported_ops = array('scheduled backup', 'manual backup',
    'remote backup', 'restore', 'list files', 'configure', 'delete',
  );

  /**
   * Save the given file to the destination.
   *
   * @see backup_migrate_destination::_save_file()
   */
  public function _save_file($file, $settings) {
    $location = $this->urlNormalize();

    $source = $file->filepath();
    $dest = $location . $file->filename();

    try {

      $source = $file->filepath();
      if (!($source_fd = fopen($source, 'rb'))) {
        throw new Exception(t("Couldn't open %file for reading.", array('%file' => $source)));
      }

      $filesize = filesize($source);

      // Before writing destination file we always need their size.
      $dest = $location . $file->filename();
      $ctx = stream_context_create();
      stream_context_set_option($ctx, array(
        'webdav'  => array('headers' => array('Content-Length' => $filesize)),
      ));
      if (!($dest_fd = fopen($dest, 'wb', FALSE, $ctx))) {
        throw new Exception(t("Couldn't open %file for writing.", array('%file' => $dest)));
      }

      while (!feof($source_fd)) {
        $buffer = fread($source_fd, 32 * 1024);
        if ($buffer === FALSE) {
          throw new Exception(t("Couldn't read %file.", array('%file' => $source)));
        }
        $written = fwrite($dest_fd, $buffer, strlen($buffer));
        if ($written === FALSE) {
          throw new Exception(t("Couldn't write %file.", array('%file' => $dest)));
        }
      }

      $ret = $file;

    }
    catch (Exception $e) {
      $ret = FALSE;
    }

    // Emulating "try-finally" block.
    if (is_resource($source_fd)) {
      fclose($source_fd);
    }
    if (is_resource($dest_fd)) {
      fclose($dest_fd);
    }

    return $ret;
  }

  /**
   * List all files from the WebDAV resource.
   *
   * @see backup_migrate_destination::_list_files()
   */
  public function _list_files() {
    backup_migrate_include('files');
    $location = $this->urlNormalize();
    $files = array();
    if (($handle = opendir($location))) {
      while (FALSE !== ($entry = readdir($handle))) {
        if (strpos($entry, '.') !== 0) {
          $filepath = $location . $entry;
          $files[$entry] = new backup_file(array('filepath' => $filepath));
        }
      }
      closedir($handle);
    }

    return $files;
  }

  /**
   * Load the file with the given destination specific id and return as a backup_file object.
   *
   * @see backup_migrate_destination::load_file()
   */
  public function load_file($file_id) {
    backup_migrate_include('files');
    $filepath = $this->urlNormalize() . $file_id;
    return new backup_file(array(
      'filepath' => $filepath,
    ));
  }

  /**
   * Delete the file with the given destination specific id.
   *
   * @see backup_migrate_destination::_delete_file()
   */
  public function _delete_file($file_id) {
    $filepath = $this->urlNormalize() . $file_id;
    unlink($filepath);
  }

  /**
   * Get the edit form for the item.
   *
   * @see backup_migrate_item::edit_form()
   */
  public function edit_form() {
    $form = parent::edit_form();

    $form['scheme']['#type'] = 'select';
    $form['scheme']['#options'] = array(
      'webdavs' => t('Secure WebDAV'),
      'webdav' => t('WebDAV'),
    );

    return $form;
  }

  /**
   * Get url with slash ending.
   *
   * @return string
   *   Url without ending slash.
   */
  public function urlNormalize() {
    return rtrim($this->location, '/') . '/';
  }

}
