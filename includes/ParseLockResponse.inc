<?php
/**
 * @file
 * Helper class for parsing LOCK request bodies.
 */

class BackupMigrateWebDAVParseLockResponse {

  /**
   * Lock token.
   *
   * @var string
   */
  protected $locktoken = '';

  /**
   * Collect lock token.
   *
   * @var bool
   */
  protected $collectLocktoken = FALSE;

  /**
   * Create instance.
   *
   * @param string $response
   *   XML response body.
   */
  public function __construct($response) {
    $xml_parser = xml_parser_create_ns('UTF-8', ' ');
    xml_set_element_handler($xml_parser, array($this, 'startElement'), array($this, 'endElement'));
    xml_set_character_data_handler($xml_parser, array($this, 'data'));
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, FALSE);

    $this->success = xml_parse($xml_parser, $response, TRUE);

    xml_parser_free($xml_parser);
  }

  /**
   * Callback for XML start element.
   *
   * @param resource $parser
   *   XML parser instance.
   * @param string $name
   *   Tag name.
   * @param array $attrs
   *   Tag attributes.
   */
  public function startElement($parser, $name, $attrs) {
    if (strstr($name, ' ')) {
      list($ns, $tag) = explode(' ', $name);
    }
    else {
      $ns = '';
      $tag = $name;
    }

    if ($ns == 'DAV:') {
      switch ($tag) {
        case 'locktoken':
          $this->collectLocktoken = TRUE;
          break;
      }
    }
  }

  /**
   * Callback for element data.
   *
   * @param resource $parser
   *   An XML parser.
   * @param string $data
   *   XML tag content.
   */
  public function data($parser, $data) {
    if ($this->collectLocktoken) {
      $this->locktoken .= $data;
    }
  }

  /**
   * Callback for XML end element.
   *
   * @param resource $parser
   *   XML parser handler.
   * @param string $name
   *   Tag element name.
   */
  public function endElement($parser, $name) {
    if (strstr($name, ' ')) {
      list(, $tag) = explode(' ', $name);
    }
    else {
      $tag = $name;
    }

    switch ($tag) {
      case 'locktoken':
        $this->collectLocktoken = FALSE;
        $this->locktoken = trim($this->locktoken);
        break;
    }
  }

}
