<?php

/**
 * @file
 * WebDAV client stream wrapper.
 *
 * This class converted to PHP5 webdav PEAR package.
 * @link http://pear.php.net/package/HTTP_WebDAV_Client
 * @author Hartmut Holzgraefe <hholzgra@php.net>
 */

/**
 * A stream wrapper class for WebDAV access.
 */
class BackupMigrateWebDAVStream implements DrupalStreamWrapperInterface {

  // WebDAV defines some addition HTTP methods.
  const HTTP_REQUEST_METHOD_GET       = 'GET';
  const HTTP_REQUEST_METHOD_COPY      = 'COPY';
  const HTTP_REQUEST_METHOD_MOVE      = 'MOVE';
  const HTTP_REQUEST_METHOD_MKCOL     = 'MKCOL';
  const HTTP_REQUEST_METHOD_PROPFIND  = 'PROPFIND';
  const HTTP_REQUEST_METHOD_PROPPATCH = 'PROPPATCH';
  const HTTP_REQUEST_METHOD_LOCK      = 'LOCK';
  const HTTP_REQUEST_METHOD_UNLOCK    = 'UNLOCK';
  const HTTP_REQUEST_METHOD_OPTIONS   = 'OPTIONS';
  const HTTP_REQUEST_METHOD_PUT       = 'PUT';
  const HTTP_REQUEST_METHOD_DELETE    = 'DELETE';

  /**
   * Stream context options.
   *
   * Options {@link http://php.net/manual/en/function.stream-context-create.php}
   * passed to {@link http://php.net/manual/en/function.fopen.php}
   * as $context parameter.
   * This property must be public so PHP can populate it with the actual
   * context resource.
   * {@link http://php.net/manual/en/class.streamwrapper.php}
   *
   * @var Resource
   */
  public $context;

  /**
   * User-Agent: header string.
   *
   * @var string
   */
  protected $userAgent = 'HTTP_WebDAV_Client';

  /**
   * Content-type: header string.
   *
   * @var string
   */
  protected $contentType = 'application/octet-stream';

  /**
   * The http or https resource URL.
   *
   * @var string
   */
  protected $url = '';

  /**
   * The resource URL path.
   *
   * @var string
   */
  protected $path = '';

  /**
   * File position indicator.
   *
   * @var int offset in bytes
   */
  protected $position = 0;

  /**
   * File status information cache.
   *
   * @var array stat information
   */
  protected $stat = array();

  /**
   * User name for authentication.
   *
   * @var string name
   */
  protected $user = '';

  /**
   * Password for authentication.
   *
   * @var string password
   */
  protected $pass = '';

  /**
   * WebDAV protocol levels supported by the server.
   *
   * @var array level entries
   * @see WebDAVClientStream::checkOptions()
   */
  protected $davLevel = NULL;

  /**
   * HTTP methods supported by the server.
   *
   * @var array method entries
   * @see WebDAVClientStream::checkOptions()
   */
  protected $davAllow = NULL;

  /**
   * Directory content cache.
   *
   * @var array filename entries
   */
  protected $dirfiles = array();

  /**
   * Current readdir() position.
   *
   * @var int
   */
  protected $dirpos = 0;

  /**
   * Remember if end of file was reached.
   *
   * @var bool
   */
  protected $eof = FALSE;

  /**
   * Lock token.
   *
   * @var string
   */
  protected $locktoken = '';

  /**
   * Stream wrapper interface open() method.
   *
   * @param string $path
   *   Resource URL.
   * @param string $mode
   *   Mode flags.
   * @param int $options
   *   Not used here.
   * @param string $opened_path
   *   Return real path here if suitable.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function stream_open($path, $mode, $options, &$opened_path) {

    // Rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    // Now get the file metadata.
    // We only need type, size, creation and modification date.
    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_PROPFIND);
    $req->addHeader('Depth', 0);
    $req->addHeader('Content-Type', 'text/xml');
    $req->data = '<?xml version="1.0" encoding="utf-8"?>
<propfind xmlns="DAV:">
<prop>
<resourcetype/>
<getcontentlength/>
<getlastmodified />
<creationdate/>
</prop>
</propfind>
';
    $data = $req->sendRequest();

    // Check the response code, anything but 207 indicates a problem.
    switch ($data->code) {
      case 200:
      case 207:
        // Now we have to parse the result to get the status info items.
        $propinfo = new BackupMigrateWebDAVParsePropfindResponse($data->data);
        $this->stat = $propinfo->stat();
        unset($propinfo);
        break;

      // Not found is ok in write modes.
      case 404:
        if (preg_match('|[aw\+]|', $mode)) {
          // Write mode.
          break;
        }
        $this->eof = TRUE;
      default:
        return FALSE;
    }

    // Mode 'w' -> open for writing, truncate existing files.
    if (strpos($mode, 'w') !== FALSE) {
      $req = $this->startRequest(self::HTTP_REQUEST_METHOD_PUT);
      $req->addHeader('Content-length', 0);
      $req->sendRequest();
    }

    // Mode 'a' -> open for appending.
    if (strpos($mode, 'a') !== FALSE) {
      $this->position = $this->stat['size'];
      $this->eof = FALSE;
    }

    return TRUE;
  }


  /**
   * Stream wrapper interface close() method.
   */
  public function stream_close() {
    // Unlock?
    if ($this->locktoken) {
      $this->stream_lock(LOCK_UN);
    }

    // Closing is simple as HTTP is stateless.
    $this->url = FALSE;

    if (isset($this->write_req)) {
      $this->write_req->reset();
      unset($this->write_req);
    }
  }

  /**
   * Stream set option interface.
   *
   * @param int $option
   *   Stream option.
   * @param int $arg1
   *   Argument for option.
   * @param int $arg2
   *   Argument for option.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function stream_set_option($option, $arg1, $arg2) {
    if ($option === 'length') {
      $this->content_length = $arg1;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Stream wrapper interface stat() method.
   *
   * @return array
   *   File information array.
   */
  public function stream_stat() {
    // We already have collected the needed information in stream_open().
    return $this->stat;
  }

  /**
   * Stream wrapper interface read() method.
   *
   * @param int $count
   *   Requested byte count.
   *
   * @return string
   *   Actual read data or FALSE on error.
   */
  public function stream_read($count) {
    $start = $this->position;
    $end = $start + $count - 1;

    // Create a GET request with a range.
    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_GET);
    $req->addHeader("Range", "bytes=$start-$end");

    $response = $req->sendRequest();
    $data = $response->data;
    $len = strlen($data);

    switch ($response->code) {
      case 200:
        // Server doesn't support range requests .
        // TODO we should add some sort of cacheing here.
        $data = substr($data, $start, $count);
        break;

      case 206:
        // Server supports range requests.
        break;

      case 416:
        // Reading beyond end of file is not an error.
        $data = '';
        $len  = 0;
        break;

      default:
        return FALSE;
    }

    // No data indicates end of file.
    if (!$len) {
      $this->eof = TRUE;
    }

    // Update position.
    $this->position += $len;

    return $data;
  }

  /**
   * Stream wrapper interface write() method.
   *
   * @param string $data
   *   Data to be written.
   *
   * @return int|FALSE
   *   Number of bytes actually written.
   */
  public function stream_write($data) {
    if (!isset($this->write_req)) {
      $this->write_req = $this->startRequest(self::HTTP_REQUEST_METHOD_PUT);
      $this->write_req->addHeader('Content-Type', $this->contentType);
      $this->write_req->addHeader('User-Agent', $this->userAgent);
      $options = stream_context_get_options($this->context);
      if (isset($options['webdav']['headers'])) {
        foreach ($options['webdav']['headers'] as $name => $value) {
          $this->write_req->addHeader($name, $value);
        }
      }
    }

    return $this->write_req->streamCopy($data);
    /*
     We do not cope with servers that do not support partial PUTs!
     And we do assume that a server does conform to the following
     rule from RFC 2616 Section 9.6:

     "The recipient of the entity MUST NOT ignore any Content-*
     (e.g. Content-Range) headers that it does not understand or
     implement and MUST return a 501 (Not Implemented) response
     in such cases."

     So the worst case scenario with a compliant server not
     implementing partial PUTs should be a failed request. A
     server simply ignoring "Content-Range" would replace
     file contents with the request body instead of putting
     the data at the requested place but we can blame it
     for not being compliant in this case ;)

     (TODO: maybe we should do a HTTP version check first?)

     we *could* emulate partial PUT support by adding local
     cacheing but for now we don't want to as it adds a lot
     of complexity and storage overhead to the client ...
    */
  }

  /**
   * Stream wrapper interface eof() method.
   *
   * @return bool
   *   Is reached eof or not.
   */
  public function stream_eof() {
    return $this->eof;
  }

  /**
   * Stream wrapper interface tell() method.
   *
   * @return int
   *   Stream position.
   */
  public function stream_tell() {
    // Just return the current position.
    return $this->position;
  }

  /**
   * Support for fflush().
   *
   * @return bool
   *   Always TRUE.
   */
  public function stream_flush() {
    return TRUE;
  }

  /**
   * Stream wrapper interface seek() method.
   *
   * @param int $pos
   *   Position to seek to.
   * @param int $whence
   *   Seek mode.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function stream_seek($pos, $whence) {
    switch ($whence) {
      case SEEK_SET:
        // Absolute position.
        $this->position = $pos;
        break;

      case SEEK_CUR:
        // Relative position.
        $this->position += $pos;
        break;

      case SEEK_END:
        // Relative position form end.
        $this->position = $this->stat['size'] + $pos;
        break;

      default:
        return FALSE;
    }

    // TODO: this is rather naive (check how libc handles this).
    $this->eof = FALSE;

    return TRUE;
  }

  /**
   * Stream wrapper interface URL stat() method.
   *
   * @param string $url
   *   URL to get stat information for.
   *
   * @return array|FALSE
   *   Stream stat or FALSE on failure.
   */
  public function url_stat($url, $flags) {
    // We map this one to open()/stat()/close().
    // There won't be much gain in inlining this.
    if (!$this->stream_open($url, 'r', array(), $dummy)) {
      return FALSE;
    }
    $stat = $this->stream_stat();
    $this->stream_close();

    return $stat;
  }

  /**
   * Stream wrapper interface opendir() method.
   *
   * @param string $path
   *   Directory resource URL.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function dir_opendir($path, $options) {
    // Rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    if (!isset($this->davAllow[self::HTTP_REQUEST_METHOD_PROPFIND])) {
      return FALSE;
    }

    // Now read the directory.
    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_PROPFIND);
    $req->addHeader('Depth', 1);
    $req->addHeader('Content-Type', 'text/xml');

    $req->data = '<?xml version="1.0" encoding="utf-8"?>
<propfind xmlns="DAV:">
<prop>
<resourcetype/>
<getcontentlength/>
<creationdate/>
<getlastmodified/>
</prop>
</propfind>
';
    $data = $req->sendRequest();

    switch ($data->code) {
      // Exception because drupal_http_request couldn't handle 207.
      case 200:
        // Multistatus content.
      case 207:
        $this->dirfiles = array();
        $this->dirpos = 0;

        // For all returned resource entries.
        foreach (explode("\n", $data->data) as $line) {
          // Preg_match_all if the whole response is one line!
          if (preg_match_all('/href>([^<]*)/', $line, $matches)) {
            // Skip the directory itself.
            foreach ($matches[1] as $match) {
              // Compare to $this->url too.
              if ($match == '' || $match == $this->path || $match == $this->url) {
                continue;
              }
              // Just remember the basenames to return them later with
              // readdir().
              $this->dirfiles[] = basename($match);
            }
          }
        }
        return TRUE;

      default:
        // Any other response state indicates an error.
        return FALSE;
    }
  }


  /**
   * Stream wrapper interface readdir() method.
   *
   * @return string|FALSE
   *   Next filename in directory or FALSE on failure.
   */
  public function dir_readdir() {
    // Bailout if directory is empty.
    if (!is_array($this->dirfiles)) {
      return FALSE;
    }

    // Bailout if we already reached end of dir.
    if ($this->dirpos >= count($this->dirfiles)) {
      return FALSE;
    }

    // Return an entry and move on.
    return $this->dirfiles[$this->dirpos++];
  }

  /**
   * Stream wrapper interface rewinddir() method.
   */
  public function dir_rewinddir() {
    // Bailout if directory content info has already been freed.
    if (!is_array($this->dirfiles)) {
      return FALSE;
    }

    // Rewind to first entry.
    $this->dirpos = 0;
  }

  /**
   * Stream wrapper interface closedir() method.
   */
  public function dir_closedir() {
    // Free stored directory content.
    if (is_array($this->dirfiles)) {
      $this->dirfiles = FALSE;
      $this->dirpos = 0;
    }
  }


  /**
   * Stream wrapper interface mkdir() method.
   *
   * @param string $path
   *   Directory path.
   * @param int $mode
   *   Directory access mode.
   * @param int $options
   *   Options.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function mkdir($path, $mode, $options) {
    // Rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_MKCOL);
    if ($this->locktoken) {
      $req->addHeader("If", "(<{$this->locktoken}>)");
    }

    // Check the response code, anything but 201 indicates a problem.
    return ($req->sendRequest()->code == 200);
  }

  /**
   * Stream wrapper interface rmdir() method.
   *
   * @param string $path
   *   Collection URL to be created.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function rmdir($path, $options) {
    // TODO: this should behave like "rmdir", currently it is more like "rm -rf"
    // rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    $req = $this->_startRequest(self::HTTP_REQUEST_METHOD_DELETE);
    if ($this->locktoken) {
      $req->addHeader("If", "(<{$this->locktoken}>)");
    }

    // Check the response code, anything but 204 indicates a problem.
    return ($req->sendRequest()->code == 204);
  }


  /**
   * Stream wrapper interface rename() method.
   *
   * @param string $path
   *   Current filename path.
   * @param string $new_path
   *   New filename path.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function rename($path, $new_path) {
    // Rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_MOVE);
    if ($this->locktoken) {
      $req->addHeader("If", "(<{$this->locktoken}>)");
    }
    if (!$this->_parse_url($new_path)) {
      return FALSE;
    }
    $req->addHeader("Destination", $this->url);
    $data = $req->sendRequest();

    // Check the response code, anything but 207 indicates a problem.
    switch ($data->code) {
      case 201:
      case 204:
        return TRUE;

      default:
        return FALSE;
    }
  }

  /**
   * Stream wrapper interface unlink() method.
   *
   * @param string $path
   *   Filename path.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function unlink($path) {
    // Rewrite the request URL.
    if (!$this->parseUrl($path)) {
      return FALSE;
    }

    // Query server for WebDAV options.
    if (!$this->checkOptions()) {
      return FALSE;
    }

    // Is DELETE supported?
    if (!isset($this->davAllow[self::HTTP_REQUEST_METHOD_DELETE])) {
      return FALSE;
    }

    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_DELETE);
    if ($this->locktoken) {
      $req->addHeader('If', "(<{$this->locktoken}>)");
    }

    return ($req->sendRequest()->code == 204);
  }


  /**
   * Static helper that registers the wrappers.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public static function register() {
    // Check that we have the required feature.
    if (!function_exists('stream_register_wrapper')) {
      return FALSE;
    }

    $wrappers = stream_get_wrappers();

    // Try to register the non-encrypted WebDAV wrapper.
    if (!in_array('webdav', $wrappers)) {
      return stream_register_wrapper('webdav', 'WebDAVClientStream', STREAM_IS_URL);
    }

    // Now try to register the SSL protocol variant
    // it is not critical if this fails
    // TODO check whether SSL is possible with HTTP_Request.
    if (!in_array('webdavs', $wrappers)) {
      stream_register_wrapper('webdavs', 'WebDAVClientStream', STREAM_IS_URL);
    }

    return TRUE;
  }


  /**
   * Helper function for URL analysis.
   *
   * @param string $path
   *   Original request URL.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  protected function parseUrl($path) {
    // Rewrite the WebDAV url as a plain HTTP url.
    $url = parse_url($path);

    // Detect whether plain or SSL-encrypted transfer is requested.
    $scheme = $url['scheme'];
    switch ($scheme) {
      case 'webdav':
        $url['scheme'] = 'http';
        break;

      case 'webdavs':
        $url['scheme'] = 'https';
        break;

      default:
        return FALSE;
    }

    if (isset($this->context)) {
      // Extract settings from stream context.
      $context = stream_context_get_options($this->context);

      // User-Agent.
      if (isset($context[$scheme]['user_agent'])) {
        $this->userAgent = $context[$scheme]['user_agent'];
      }

      // Content-Type.
      if (isset($context[$scheme]['content_type'])) {
        $this->contentType = $context[$scheme]['content_type'];
      }

      // TODO check whether to implement other HTTP specific
      // context settings from http://php.net/manual/en/context.http.php .
    }

    // If a TCP port is specified we have to add it after the host.
    if (isset($url['port'])) {
      $url['host'] .= ":$url[port]";
    }

    // Store the plain path for possible later use.
    $this->path = $url['path'];

    // Now we can put together the new URL.
    $this->url = $url['scheme'] . '://';

    // Extract authentication information.
    if (isset($url['user'])) {
      $this->user = urldecode($url['user']);
      $this->url .= $this->user;
    }
    if (isset($url['pass'])) {
      $this->pass = urldecode($url['pass']);
      $this->url .= ':' . $this->pass;
    }
    if (isset($url['user']) || isset($url['pass'])) {
      $this->url .= '@';
    }

    $this->url .= $url['host'] . $url['path'];

    return TRUE;
  }

  /**
   * Helper function for WebDAV OPTIONS detection.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  protected function checkOptions() {
    // Now check OPTIONS reply for WebDAV response headers.
    $req = $this->startRequest(self::HTTP_REQUEST_METHOD_OPTIONS);
    $data = $req->sendRequest();

    if ($data->code != 200) {
      return FALSE;
    }

    // Get the supported DAV levels and extensions.
    $dav = $this->getResponseHeader($data, 'DAV');
    $this->davLevel = array();
    foreach (explode(',', $dav) as $level) {
      $this->davLevel[trim($level)] = TRUE;
    }
    if (!isset($this->davLevel['1'])) {
      // We need at least DAV Level 1 conformance.
      return FALSE;
    }

    // Get the supported HTTP methods.
    // TODO: these are not checked for WebDAV compliance yet.
    $allow = $this->getResponseHeader($data, 'Allow');
    $this->davAllow = array();
    foreach (explode(',', $allow) as $method) {
      $this->davAllow[trim($method)] = TRUE;
    }

    // TODO: check for required WebDAV methods.
    return TRUE;
  }


  /**
   * Stream handler interface lock() method (experimental ...).
   *
   * @param int $mode
   *   Lock mode.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function stream_lock($mode) {
    // TODO: think over how to refresh locks.
    $ret = FALSE;

    // LOCK is only supported by DAV Level 2.
    if (!isset($this->davLevel['2'])) {
      return FALSE;
    }

    switch ($mode & ~LOCK_NB) {
      case LOCK_UN:
        if ($this->locktoken) {
          $req = $this->startRequest(self::HTTP_REQUEST_METHOD_UNLOCK)
                  ->addHeader('Lock-Token', "<{$this->locktoken}>")
                  ->sendRequest();

          $ret = ($req->code == 204);
        }
        break;

      case LOCK_SH:
      case LOCK_EX:
        $body = sprintf('<?xml version="1.0" encoding="utf-8" ?> 
<D:lockinfo xmlns:D="DAV:"> 
<D:lockscope><D:%s/></D:lockscope> 
<D:locktype><D:write/></D:locktype> 
<D:owner>%s</D:owner> 
</D:lockinfo>',
                        ($mode & LOCK_SH) ? "shared" : "exclusive",
                        // TODO better owner string.
                        get_class($this));
        $req = $this->startRequest(self::HTTP_REQUEST_METHOD_LOCK);
        if ($this->locktoken) {
          // Needed for refreshing a lock.
          $req->addHeader('Lock-Token', "<{$this->locktoken}>");
        }
        $req->addHeader('Timeout', 'Infinite, Second-4100000000');
        $req->addHeader('Content-Type', 'text/xml; charset="utf-8"');
        $req->data = $body;
        $data = $req->sendRequest();

        $ret = ($data->code == 200);

        if ($ret) {
          $propinfo = new BackupMigrateWebDAVParseLockResponse($data->data);
          $this->locktoken = $propinfo->locktoken;
          // TODO: deal with timeout.
        }
        break;

      default:
        break;
    }

    return $ret;
  }

  /**
   * Get HTTP request object.
   *
   * @param string $method
   *   HTTP request method name.
   *
   * @return BackupMigrateWebDAVHTTPRequest
   *   Instance of request object.
   */
  protected function startRequest($method) {

    $request = new BackupMigrateWebDAVHTTPRequest($this->url);
    $request->method = $method;

    // Setup default http headers.
    $request->addHeader('User-Agent', $this->userAgent);
    $request->addHeader('Content-Type', $this->contentType);

    return $request;
  }

  /**
   * Returns either the named header or all if no name given.
   *
   * @param object $request
   *   Request object instance.
   * @param string $headername
   *   HTTP header name.
   *
   * @return string|bool
   *   Header value if it exists in object instance or FALSE otherwise.
   *
   * @see BackupMigrateWebDAVHTTPRequest::sendRequest()
   */
  protected function getResponseHeader($request, $headername = NULL) {
    if (!isset($headername)) {
      return isset($request->headers) ? $request->headers : array();
    }
    else {
      $headername = strtolower($headername);
      return isset($request->headers[$headername]) ? $request->headers[$headername] : FALSE;
    }
  }

  /**
   * Set the absolute stream resource URI.
   *
   * @see DrupalStreamWrapperInterface::setUri()
   */
  public function setUri($uri) {
    $this->url = $uri;
  }

  /**
   * Returns the stream resource URI.
   *
   * @see DrupalStreamWrapperInterface::getUri()
   */
  public function getUri() {
    return $this->url;
  }

  /**
   * Returns a web accessible URL for the resource.
   *
   * @see DrupalStreamWrapperInterface::getExternalUrl()
   */
  public function getExternalUrl() {
    return $this->url;
  }

  /**
   * Returns the MIME type of the resource.
   *
   * @see DrupalStreamWrapperInterface::getMimeType()
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return $this->contentType;
  }

  /**
   * Changes permissions of the resource.
   *
   * @see DrupalStreamWrapperInterface::chmod()
   */
  public function chmod($mode) {
    // TODO: can webdav allow this?
    return FALSE;
  }

  /**
   * Base implementation of realpath().
   *
   * @see DrupalStreamWrapperInterface::realpath()
   */
  public function realpath() {
    return $this->url;
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * @see DrupalStreamWrapperInterface::dirname()
   */
  public function dirname($uri = NULL) {
    return $this->url;
  }

}
