<?php
/**
 * @file
 * Helper class for parsing PROPFIND response bodies.
 */

/**
 * Get requested properties as array containing name/namespace pairs.
 */
class BackupMigrateWebDAVParsePropfindResponse {

  /**
   * Temporary buffer.
   *
   * @var string
   */
  private $tmpdata = NULL;

  /**
   * Create instance.
   *
   * @param string $response
   *   XML data response.
   */
  public function __construct($response) {
    $this->urls = array();

    $this->depth = 0;

    $xml_parser = xml_parser_create_ns('UTF-8', ' ');
    xml_set_element_handler($xml_parser, array($this, 'startElement'), array($this, 'endElement'));
    xml_set_character_data_handler($xml_parser, array($this, 'data'));
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, FALSE);
    $this->success = xml_parse($xml_parser, $response, TRUE);
    xml_parser_free($xml_parser);

    unset($this->depth);
  }

  /**
   * Callback for XML start element.
   *
   * @param resource $parser
   *   XML parser instance.
   * @param string $name
   *   Tag name.
   * @param array $attrs
   *   Tag attributes.
   */
  public function startElement($parser, $name, $attrs) {
    if (strstr($name, ' ')) {
      list($ns, $tag) = explode(' ', $name);
      if ($ns == '') {
        $this->success = FALSE;
      }
    }
    else {
      $ns = '';
      $tag = $name;
    }

    switch ($this->depth) {
      case '2':
        switch ($tag) {
          case 'propstat':
            // TODO check is_executable, lockinfo ...
            $this->tmpprop = array('mode' => 0100666 /* all may read and write (for now) */);
            break;
        }
    }

    $this->depth++;
  }

  /**
   * Callback for XML end element.
   *
   * @param resource $parser
   *   XML parser handler.
   * @param string $name
   *   Tag element name.
   */
  public function endElement($parser, $name) {
    if (strstr($name, ' ')) {
      list($ns, $tag) = explode(' ', $name);
      if ($ns == '') {
        $this->success = FALSE;
      }
    }
    else {
      $ns = '';
      $tag = $name;
    }

    $this->depth--;

    switch ($this->depth) {
      case '1':
        if ($tag == 'response') {
          $this->urls[$this->tmphref] = $this->tmpvals;
          unset($this->tmphref);
          unset($this->tmpvals);
        }
        break;

      case '2':
        if ($tag == 'href') {
          $this->tmphref = $this->tmpdata;
        }
      case 'propstat':
        if (isset($this->tmpstat) && strstr($this->tmpstat, ' 200 ')) {
          $this->tmpvals = $this->tmpprop;
        }
        unset($this->tmpstat);
        unset($this->tmpprop);
        break;

      case '3':
        if ($tag == 'status') {
          $this->tmpstat = $this->tmpdata;
        }
      case '4':
        if ($this->tmpdata) {
          switch ($tag) {
            case 'getlastmodified':
              $this->tmpprop['atime'] = strtotime($this->tmpdata);
              $this->tmpprop['mtime'] = strtotime($this->tmpdata);
              break;

            case 'creationdate':
              $t = preg_split('/[^[:digit:]]/', $this->tmpdata);
              $this->tmpprop['ctime'] = mktime($t[3], $t[4], $t[5], $t[1], $t[2], $t[0]);
              unset($t);
              break;

            case 'getcontentlength':
              $this->tmpprop['size'] = $this->tmpdata;
              break;
          }
        }
      case '5':
        if ($tag == 'collection') {
          // Clear S_IFREG.
          $this->tmpprop['mode'] &= ~0100000;
          // Set S_IFDIR.
          $this->tmpprop['mode'] |= 040000;
        }
    }

    $this->tmpdata = NULL;
  }

  /**
   * Callback for element data.
   *
   * @param resource $parser
   *   An XML parser.
   * @param string $data
   *   XML tag content.
   */
  public function data($parser, $data) {
    $this->tmpdata = $data;
  }

  /**
   * Current stat element.
   */
  public function stat($href = FALSE) {
    reset($this->urls);
    return current($this->urls);
  }

}
