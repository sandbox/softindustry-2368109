<?php

/**
 * @file
 * A filter for restoring backups from WebDAV service.
 */

/**
 * A filter for restoring backups from WebDAV.
 *
 * Task of this filter is copy backup archive from remote source to local
 * side before backup restoring. Why? Compression filter directly open
 * archive with gzopen() and filename as webdav://.... scheme. This lead to
 * using stream_cast() function but webdav stream does not support it.
 * To resolve this issue, archive copied to local side and unpacked as local.
 *
 * @todo More investigate gzopen(), stream_cast() issues.
 * @ingroup backup_migrate_filters
 */
class BackupMigrateWebDAVFilterRestore extends backup_migrate_filter {
  public $op_weights = array('backup' => 100, 'restore' => -99);

  /**
   * Processed backups.
   *
   * @var array
   */
  protected $files = array();

  /**
   * Implements pre_restore filter hook.
   *
   * For webdav backups copy them to local private directory.
   *
   * @see backup_migrate_filter::pre_restore()
   */
  public function pre_restore($file, $settings) {
    if (!$this->isWebdav($file->path)) {
      return FALSE;
    }

    $dest = 'private://backup_migrate/webdav';
    if (!is_dir($dest) && !file_prepare_directory($dest, FILE_CREATE_DIRECTORY)) {
      return FALSE;
    }

    $filename = $file->file_info['filename'];
    $dest = $dest . '/' . $filename;
    if (file_unmanaged_copy($file->path, $dest, FILE_EXISTS_REPLACE)) {
      $file->path = $dest;
      $this->files[$filename] = $dest;
    }
  }

  /**
   * Implements post_restore filter hook.
   *
   * Delete copied archive.
   *
   * @see backup_migrate_filter::post_restore()
   */
  public function post_restore($file, $settings) {
    $filename = $file->file_info['filename'];
    if (isset($this->files[$filename]) && file_exists($this->files[$filename])) {
      if (unlink($this->files[$filename])) {
        unset($this->files[$filename]);
      }
    }
  }

  /**
   * Check whether url is webdav wrapper.
   *
   * @param string $url
   *   Absolute url.
   *
   * @return bool
   *   Whether url started with webdav(s) scheme or not.
   */
  protected function isWebdav($url) {
    if (!($uri = parse_url($url))) {
      return FALSE;
    }
    return (isset($uri['scheme']) && ($uri['scheme'] == 'webdav' ||
        $uri['scheme'] == 'webdavs'));
  }

}
