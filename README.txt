INTRODUCTION
------------
This module provides a destination for the Backup and Migrate module which is
aimed utilize the WebDAV storage systems (ownCloud, YandexDisk, ...).


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/softindustry/2368109
   
   
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2368109
   

REQUIREMENTS
------------
Depends on the 3d version of the Backup and Migrate module 7.x-3.x
(https://www.drupal.org/project/backup_migrate).


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
Configure the backup destination. Open Configuration » Backup and Migrate » 
Settings » Destinations (admin/config/system/backup_migrate/settings/destination)
and click the "Add destination" link. From the list "Offsite Destinations" 
choose "WebDAV".


There you will find:
 * Secure WebDAV/WebDAV - which uses https or http protocol.
 * Destinations name - desired backup name
 * Host - host is a domain and not an url
 * Path - directory on the webdav service
 * Username/Password - credentials for webdav connection


EXAMPLE OF OWNCLOUD CONFIGURATION
---------------------------------
Open your real ownCloud link, login and create a backup directory if necessary. 
Then, click the grey gear under the "Deleted files" link in the left bottom 
corner. You will see a text field with the absolute URL to access your files 
via WebDAV. Copy this URL. For example, the url is: 
(https://mycloud.company.com/remote.php/webdav/)


Open Backup and Migrate WebDAV destination settings.
 * Choose Secure WebDAV, because protocol is https.
 * Host - mycloud.company.com
 * Path - /remote.php/webdav/ is your root directory on ownCloud. If you
           intend to save backups to the "Backup" directory (it should already
           exists), add "Backup" in the end of the path:
           /remote.php/webdav/Backup
 * Username/Password - login/password to your ownCloud


VERY IMPORTANT NOTE
-------------------
 - Please, make sure that your php timeout parameter (max_execution_time) gives
   you enough time to backup your site and copy it to the remote destination!
 - Don't rely on remote backups. It would be better also to make local and
   manual backups, copy them to removable media, etc...


MAINTAINERS
-----------
Current maintainers:
 * SoftIndustry (softindustry) - https://www.drupal.org/user/3083991
 * Skorobogatko Alexei (skoro) - https://www.drupal.org/user/2702559
